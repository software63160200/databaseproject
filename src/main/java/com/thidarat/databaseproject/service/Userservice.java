/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.databaseproject.service;

import com.thidarat.databaseproject.dao.UserDao;
import com.thidarat.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class Userservice {
    public User login(String name ,String password){
        UserDao userDao =new UserDao();
        User user =userDao.getByName(name);
        if(user !=null && user.getPassword().equals(password)){
            return user;
        }
        
        return null;
    }
}
