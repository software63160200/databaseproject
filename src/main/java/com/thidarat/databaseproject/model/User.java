/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class User { //๋Java Class
    //สร้างตามAttribute ใน SQLite ตาราง User

    private int id;
    private String name;
    private String password;
    private int role;
    private String gander;

    public User(int id, String name, String password, int role, String gander) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.gander = gander;
    }

    public User(String name, String password, int role, String gander) {
        this.id = -1; //เริ่มต้นไม่มีไอดี
        this.name = name;
        this.password = password;
        this.role = role;
        this.gander = gander;
    }

    public User() {
        this.id = -1; //เริ่มต้นไม่มีไอดี
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getGander() {
        return gander;
    }

    public void setGander(String gander) {
        this.gander = gander;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", password=" + password + ", role=" + role + ", gander=" + gander + '}';
    }

    public void getGander(String m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("user_id"));
            user.setName(rs.getString("user_name"));
            user.setPassword(rs.getString("user_password"));
            user.setRole(rs.getInt("user_role"));
            user.setGander(rs.getString("user_gender"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);       
            return null;
        }
        return user;

    }

}
