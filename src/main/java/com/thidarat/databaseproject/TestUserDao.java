/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.databaseproject;

import com.thidarat.databaseproject.dao.UserDao;
import com.thidarat.databaseproject.helper.DatabaseHelper;
import com.thidarat.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();

        // Selection
        //System.out.println(userDao.getAll());  // ปริ้น user ทั้งหมด
        for (User u : userDao.getAll()) { // ปริ้น user ทั้งหมด
            System.out.println(u);
        }
        System.out.println("--------------------------------------");
//
//        // Selection
//        User user1 = userDao.get(2); // กำหนด id  user ที่ต้องการหา
//        System.out.println(user1);
//        System.out.println("--------------------------------------");
//
//        //เพิ่ม user
////        User newUser = new User("user3", "password", 2, "F");
////        User insetedUser = userDao.save(newUser);
////        System.out.println(insetedUser);
////        System.out.println("--------------------------------------");
////        
////        // อัพเดต User
////        insetedUser.setGander("M");
//        user1.setGander("M");
//        userDao.update(user1);
//        User updateuer = userDao.get(user1.getId()); // updata  user 
//        System.out.println(updateuer);
//        System.out.println("--------------------------------------");
//        
//        // Delete User
//        userDao.delete(user1);
//        for (User u : userDao.getAll()) { // ปริ้น user ทั้งหมด
//            System.out.println(u);
//        }
//        System.out.println("--------------------------------------");
//        
    for (User u : userDao.getAll("user_name like 'u%' ", "user_name asc , user_gender")) { // ปริ้น user ทั้งหมด
            System.out.println(u);
        }

        
        DatabaseHelper.close(); // ปิด
    }
}
