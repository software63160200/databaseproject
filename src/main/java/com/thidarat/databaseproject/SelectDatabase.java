/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class SelectDatabase {

    public static void main(String[] args) {
        Connection conn = null; // การConnect
        String url = "jdbc:sqlite:dcoffee.db";
        
        // Connection Database
        try {

            conn = DriverManager.getConnection(url); // จัดการต่อ jdbc
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
    
        }
        
        // Selection
        String sql ="SELECT * FROM category"; // เรียกดูใน category ทั้งหมด 
        
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) { // การวนปริ้นใน category
                
                System.out.println(rs.getInt("category_id")+ " "
                        +rs.getString("category_name"));
                
            }
            
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
        }
        
    
        //Close Database
        
        if (conn != null) { //ถ้า conn ไม่ null
            try {
                conn.close(); // ปิด
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }

        }
    }
    }
