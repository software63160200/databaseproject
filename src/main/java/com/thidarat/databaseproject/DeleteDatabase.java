/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author acer
 */
public class DeleteDatabase {
    public static void main(String[] args) {
          Connection conn = null; // การConnect
        String url = "jdbc:sqlite:dcoffee.db";
        
        // Connection Database
        try {

            conn = DriverManager.getConnection(url); // จัดการต่อ jdbc
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
    
        }
        
        // Delete
        String sql ="DELETE FROM category  WHERE category_id=?"; // เพิ่มข้อมูลใน category
        
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
             stmt.setInt(1, 3); // category_id = 3
           
            int status = stmt.executeUpdate();
//            ResultSet key =stmt.getGeneratedKeys();
//            key.next();
//            System.out.println(""+key.getInt(1));
   
            
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
        }
        
    
        //Close Database
        
        if (conn != null) { //ถ้า conn ไม่ null
            try {
                conn.close(); // ปิด
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }

        }
    }
}
