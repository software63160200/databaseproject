/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.databaseproject.dao;

import com.thidarat.databaseproject.helper.DatabaseHelper;
import com.thidarat.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class UserDao implements Dao<User> { //๋ Java Class

    @Override
    public User get(int id) { // ดู ข้อมูล user คนเดียวด้วย id
        User user = null;

        // Selection
        String sql = "SELECT * FROM user WHERE user_id=?"; // เรียกดูใน user ที่กำหนด
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) { // การวนปริ้นใน user
                user = User.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;

    }
    

    public User getByName(String name) { // ดู ข้อมูล user คนเดียวด้วย id
        User user = null;

        // Selection
        String sql = "SELECT * FROM user WHERE user_id=?"; // เรียกดูใน user ที่กำหนด
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) { // การวนปริ้นใน user
                user = User.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;

    }

    @Override
    public List<User> getAll() { //ดูข้อมูล user ทั้งหมด ในตาราง user 
        ArrayList<User> list = new ArrayList();

        // Selection
        String sql = "SELECT * FROM user"; // เรียกดูใน user ทั้งหมด 
        Connection conn = DatabaseHelper.getConnect();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) { // การวนปริ้นใน user
                User user = User.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList();

        // Selection
        String sql = "SELECT * FROM user  where " + where + " ORDER BY  " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) { // การวนปริ้นใน user
                User user = User.fromRS(rs);

                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public User save(User obj) {

        // Insert
        String sql = "INSERT INTO user ("
                + " user_name,"
                + " user_gender,"
                + " user_password,"
                + "user_role )"
                + "VALUES (?,?,?,?)"; // เพิ่มข้อมูล user
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGander());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());

            //  System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public User update(User obj) {
        // Update
        String sql = "UPDATE user SET user_name = '?',user_gender = '?',user_password = '?',user_role = '?' "
                + "WHERE user_id = '?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGander());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.setInt(5, obj.getId());

            //  System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(User obj) {
        // Delete
        String sql = "DELETE FROM user WHERE user_id=?"; // เรียกดูใน user ที่กำหนด
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

}
